
#include <Wire.h>
#include <MPU6050.h>
#include <KalmanFilter.h>
#include <KalmanFilter1.h>
#include <KalmanFilter2.h>
#include <I2Cdev.h>

// Libraries for rfComm
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

/* RFCOMM SETUP */
// create RF24 object
RF24 radio(9,8);          // CE and CSN pins
// communication address
const byte address[6] = "00001";
// calculated pitch from all 3 IMUs
float sensor_data[3] = {0,0,0};

/* IMU SETUP */
MPU6050 mpu;

KalmanFilter kalmanX(0.001, 0.003, 0.03);
KalmanFilter kalmanY(0.001, 0.003, 0.03);

KalmanFilter1 kalmanX1(0.001, 0.003, 0.03);
KalmanFilter1 kalmanY1(0.001, 0.003, 0.03);

KalmanFilter2 kalmanX2(0.001, 0.003, 0.03);
KalmanFilter2 kalmanY2(0.001, 0.003, 0.03);

float accPitch = 0;
float accRoll = 0;

float kalPitch = 0;
float kalRoll = 0;

float accPitch2 = 0;
float accRoll2 = 0;

float kalPitch2 = 0;
float kalRoll2 = 0;

float accPitch3 = 0;
float accRoll3 = 0;

float kalPitch3 = 0;
float kalRoll3 = 0;

unsigned long CurrentTime;
unsigned long StartTime;

int RegisterMode;

void setup() 
{
  /* RFCOMM */
  radio.begin();
  // set address
  radio.openWritingPipe(address);
  // set module as transmitter
  radio.stopListening();

  /* IMU SETUP */  
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  Serial.begin(115200);
  
  digitalWrite(4, LOW);
  digitalWrite(5,HIGH);
  digitalWrite(6,HIGH);
  //Uncomment this to add a high pass filter
  //mpu.setDLPFMode(MPU6050_DLPF_2);
  //mpu.setDLPFMode(0b010);
  //Initialize MPU6050
  while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))
  {
    Serial.print("MPU1 Not Found");
    delay(500);
  }
  mpu.calibrateGyro();

  digitalWrite(4, HIGH);
  digitalWrite(5,LOW);
  digitalWrite(6,HIGH);
  //mpu.setDLPFMode(0b010);
  //Uncomment this to add a high pass filter
  //mpu.setDHPFMode(MPU6050_DHPF_5HZ);
    // Initialize MPU6050
  while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))
  {
    Serial.print("MPU2 Not Found");
    delay(500);
  }
  mpu.calibrateGyro();
 
  digitalWrite(4, HIGH);
  digitalWrite(5,HIGH);
  digitalWrite(6,LOW);
  //mpu.setDLPFMode(0b010);
  //look at the cpp for mpu6050 to change 0b010 to diferent frequency/see what this means

  // Initialize MPU6050
  while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))
  {
    Serial.print("MPU3 Not Found");
    delay(500);
  }
 
  // Calibrate gyroscope. The calibration must be at rest.
  // If you don't want calibrate, comment this line.
  mpu.calibrateGyro();
  StartTime=millis();
}

void loop()
{  
    CurrentTime=millis()-StartTime;
    //Serial.print(CurrentTime);
    //Serial.print(",");
  //ACCELEROMETER 1: -----------------------------------------------------------------------------------------------------------
     //Serial.print("ACCELEROMETER 1:  ");
     digitalWrite(4, LOW);
     digitalWrite(5,HIGH);
     digitalWrite(6,HIGH);
     Vector acc = mpu.readNormalizeAccel();
     Vector gyr = mpu.readNormalizeGyro();

     // Calculate Pitch & Roll from accelerometer (deg)
     accPitch = -(atan2(acc.XAxis, sqrt(acc.YAxis*acc.YAxis + acc.ZAxis*acc.ZAxis))*180.0)/M_PI;
     accRoll  = (atan2(acc.YAxis, acc.ZAxis)*180.0)/M_PI;

     // Kalman filter
     kalPitch = -kalmanY.update(accPitch, gyr.YAxis);
     kalRoll = kalmanX.update(accRoll, gyr.XAxis);

      //Serial.print(accPitch);
      //Serial.print(",");
      //Serial.print(accRoll);
      //Serial.print(",");
      Serial.print(kalPitch);
      Serial.print(",");
     // Serial.print(kalRoll);
     // Serial.print(",");
      //Serial.print(acc.XAxis);
      //Serial.print(",");
      //Serial.print(acc.YAxis);
      //Serial.print(",");
      //Serial.print(acc.ZAxis);
      //Serial.print(",");
      //Serial.print(gyr.XAxis);
      //Serial.print(",");
      //Serial.print(gyr.YAxis);
      //Serial.print(",");
      //Serial.print(gyr.ZAxis);
      
    //ACCELEROMETER 2 -----------------------------------------------------------------------------------------------------------
      //Serial.print("   ACCELEROMETER 2:");
      digitalWrite(4, HIGH);
      digitalWrite(5,LOW);
      digitalWrite(6,HIGH);
      Vector acc2 = mpu.readNormalizeAccel();
      Vector gyr2 = mpu.readNormalizeGyro();

      // Calculate Pitch & Roll from accelerometer (deg)
      accPitch2 = -(atan2(acc2.YAxis, acc2.ZAxis)*180.0)/M_PI-90;
      accRoll2  = -(atan2(acc2.XAxis, sqrt(acc2.YAxis*acc2.YAxis + acc2.ZAxis*acc2.ZAxis))*180.0)/M_PI;

      // Kalman filter
      kalPitch2 = kalmanY1.update(accPitch2, gyr2.YAxis);
      kalRoll2 = kalmanX1.update(accRoll2, gyr2.XAxis);

      //Serial.print(accPitch2);
      //Serial.print(",");
      //Serial.print(accRoll2);
      //Serial.print(",");
      //Serial.print(kalPitch2);
      //Serial.print(",");
     // Serial.print(kalRoll2);
     // Serial.print(",");
      //Serial.print(acc2.XAxis);
      //Serial.print(",");
      //Serial.print(acc2.YAxis);
      //Serial.print(",");
      Serial.print(acc2.ZAxis);
      Serial.print(",");
      //Serial.print(gyr2.XAxis);
      //Serial.print(",");
     // Serial.print(gyr2.YAxis);
      //Serial.print(",");
      //Serial.print(gyr2.ZAxis);
     
  //ACCELEROMETER 3 -----------------------------------------------------------------------------------------------------------
      //Serial.print("   ACCELEROMETER 3:");
      digitalWrite(4,HIGH);
      digitalWrite(5,HIGH);
      digitalWrite(6,LOW);
      Vector acc3 = mpu.readNormalizeAccel();
      Vector gyr3 = mpu.readNormalizeGyro();

      // Calculate Pitch & Roll from accelerometer (deg)
      accPitch3 = -(atan2(acc3.XAxis, sqrt(acc3.YAxis*acc3.YAxis + acc3.ZAxis*acc3.ZAxis))*180.0)/M_PI;
      accRoll3  = (atan2(acc3.YAxis, acc3.ZAxis)*180.0)/M_PI;

      // Kalman filter
      kalPitch3 = kalmanY2.update(accPitch3, gyr3.YAxis);
      kalRoll3 = kalmanX2.update(accRoll3, gyr3.XAxis);


      //Serial.print(accPitch3);
      //Serial.print(",");
      //Serial.print(accRoll3);
      //Serial.print(",");
      Serial.print(kalPitch3);
      Serial.print(",");
      //Serial.print(kalRoll3);
     // Serial.print(",");
      //Serial.print(acc3.XAxis);
      //Serial.print(",");
      //Serial.print(acc3.YAxis);
      //Serial.print(",");
      //Serial.print(acc3.ZAxis);
      //Serial.print(",");
      //Serial.print(gyr3.XAxis);
      //Serial.print(",");
      //Serial.print(gyr3.YAxis);
      //Serial.print(",");
      //Serial.print(gyr3.ZAxis);


  Serial.println();

  // Send data over radio
  sensor_data[0] = kalPitch;
  sensor_data[1] = acc2.ZAxis;
  sensor_data[2] = kalPitch3;
  radio.write(&sensor_data, sizeof(sensor_data));
  
}
